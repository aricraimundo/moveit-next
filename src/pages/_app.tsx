import '../styles/global.css'

// rendered in all screens
function MyApp({ Component, pageProps }) {
  return (
    <Component {...pageProps} />
  )
}

export default MyApp;
